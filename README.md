# ITP 100 Final Project - Enigma Simulator

### Download
To run, please download the [final_webpage](https://gitlab.com/jwploetz/itp100finalproject/-/tree/master/final_webpage) directory and open CipherMachineHome.html in a browser.

This project is a simulator of the Enigma Cipher, which was used in WWII by the German army. It is a very powerful cipher which used multiple forms of substitution along with constantly changing cipher settings. There are 103 sextillion different combinations of settings that can be created. This machine's cipher was broken by the British army around 1941 with the help of the Polish army. The intelligence gained from the breaking of the Enigma cipher brought the war to a close an estimated two years early. This is one of the most powerful cipher machines ever produced, if not the most powerful. Read more about it in the History page on our website.

## Description  
This project is a collaboration between Ploetz and JFK. For the project, we are making a simulator of the Enigma, the German WW2 cipher machine, and incorporating them into a webpage. Jack is good at complex logic, and other Jack is good at webpage design, so we feel that a partnership between us will greatly increase the quality of our work, and provide a valuable learning opportunity for both of us. We will be using Python, HTML, CSS, and [Brython](https://brython.info/). Brython (Browser Python) allows you to use Python inside of HTML5 `<script>` tags, just like you would with JavaScript.  

## History


## Images  
